﻿using AutoMapper;

namespace EvolutionXp.PiedraPapelTijera.Api.Controllers.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            // game
            CreateMap<Model.Dtos.GameDto, Model.Entities.Game>();
            CreateMap<Model.Entities.Game, Model.Dtos.GameDto>();

            // player
            CreateMap<Model.Dtos.PlayerDto, Model.Entities.Player>();
            CreateMap<Model.Entities.Player, Model.Dtos.PlayerDto>();

            // round
            CreateMap<Model.Dtos.RoundDto, Model.Entities.Round>();
            CreateMap<Model.Entities.Round, Model.Dtos.RoundDto>();

            // move type
            CreateMap<Model.Dtos.MoveTypeDto, Model.Entities.MoveType>();
            CreateMap<Model.Entities.MoveType, Model.Dtos.MoveTypeDto>();

            // round result
            CreateMap<Model.Dtos.RoundResultDto, Model.Entities.RoundResult>();
            CreateMap<Model.Entities.RoundResult, Model.Dtos.RoundResultDto>();
        }
    }
}
