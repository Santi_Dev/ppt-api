﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EvolutionXp.PiedraPapelTijera.Api.Controllers.Controllers.Api.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        #region Attributes

        private readonly Domain.GameService.IGameService GameService;

        #endregion

        #region Constructor

        public GamesController(Domain.GameService.IGameService gameServiceInstance)
        {
            GameService = gameServiceInstance;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create new game
        /// </summary>
        /// <param name="gameInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Model.Services.GeneralResponse<Model.Dtos.GameDto>> AddGameAsync([FromBody] Model.Dtos.GameDto gameInfo)
        {
            return await GameService.AddGameAsync(gameInfo);
        }

        /// <summary>
        /// Get game by id
        /// </summary>
        /// <param name="gameId">Game id</param>
        /// <returns></returns>
        [HttpGet("{gameId}")]
        public async Task<Model.Services.GeneralResponse<Model.Dtos.GameDto>> GetGameByIdAsync(string gameId)
        {
            return await GameService.GetGameByIdAsync(gameId);
        }

        /// <summary>
        /// Reset game
        /// </summary>
        /// <param name="gameId">Game id</param>
        /// <returns></returns>
        [HttpPut("{gameId}/reset")]
        public async Task<Model.Services.BaseResponse> ResetGameAsync(string gameId)
        {
            return await GameService.ResetGameAsync(gameId);
        }

        #endregion
    }
}