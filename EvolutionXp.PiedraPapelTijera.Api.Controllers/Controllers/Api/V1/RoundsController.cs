﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EvolutionXp.PiedraPapelTijera.Api.Controllers.Controllers.Api.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RoundsController : ControllerBase
    {
        #region Attributes

        private readonly Domain.RoundService.IRoundService RoundService;

        #endregion

        #region Constructor

        public RoundsController(Domain.RoundService.IRoundService roundServiceInstance)
        {
            RoundService = roundServiceInstance;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create new round
        /// </summary>
        /// <param name="roundInfo">Round info</param>
        /// <returns>Base response <see cref="Model.Services.BaseResponse"/></returns>
        [HttpPost]
        public async Task<Model.Services.BaseResponse> AddRoundAsync([FromBody] Model.Dtos.RoundDto roundInfo)
        {
            return await RoundService.AddRoundAsync(roundInfo);
        }

        #endregion
    }
}