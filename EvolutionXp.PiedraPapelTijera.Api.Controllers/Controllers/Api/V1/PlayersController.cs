﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EvolutionXp.PiedraPapelTijera.Api.Controllers.Controllers.Api.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        #region Attributes

        private readonly Domain.PlayerService.IPlayerService PlayerService;

        #endregion

        #region Constructor

        public PlayersController(Domain.PlayerService.IPlayerService playerServiceInstance)
        {
            PlayerService = playerServiceInstance;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get player winner
        /// </summary>
        /// <param name="gameId">Game id</param>
        /// <returns>General response with winner <see cref="Model.Services.GeneralResponse{T}"/></returns>
        [HttpGet("games/{gameId}/winner")]
        public async Task<Model.Services.GeneralResponse<Model.Dtos.PlayerDto>> GetPlayerWinnerByGameAsync(string gameId)
        {
            return await PlayerService.GetPlayerWinnerByGameAsync(gameId);
        }

        #endregion
    }
}