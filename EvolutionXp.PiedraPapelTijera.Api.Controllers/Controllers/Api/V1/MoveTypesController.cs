﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EvolutionXp.PiedraPapelTijera.Api.Controllers.Controllers.Api.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MoveTypesController : ControllerBase
    {
        #region Attributes

        private readonly Domain.MoveTypeService.IMoveTypeService MoveTypeService;

        #endregion

        #region Constructor

        public MoveTypesController(Domain.MoveTypeService.IMoveTypeService moveTypeServiceInstance)
        {
            MoveTypeService = moveTypeServiceInstance;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get all move types
        /// </summary>
        /// <returns>General response with move types <see cref="Model.Services.GeneralResponse{T}"/></returns>
        [HttpGet]
        public async Task<Model.Services.GeneralResponse<Model.Dtos.MoveTypeDto>> GetMoveTypesAsync()
        {
            return await MoveTypeService.GetMoveTypesAsync();
        }

        #endregion
    }
}