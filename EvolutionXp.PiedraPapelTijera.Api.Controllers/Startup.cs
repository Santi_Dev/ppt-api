using EvolutionXp.PiedraPapelTijera.Api.Domain;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure;
using EvolutionXp.PiedraPapelTijera.Api.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;

namespace EvolutionXp.PiedraPapelTijera.Api.Controllers
{
    public class Startup
    {

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"responseMessages.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            configuration = builder.Build();

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var connectionStrings = Configuration.GetConnectionString("DefaultConnection");

            // add layers startups configuration
            services.AddServices();
            services.AddModels(connectionStrings);
            services.AddInfrastructure(Configuration);

            // configure infrastructure response messages
            services.Configure<Infrastructure.Messages.ResponseMessages>(Configuration.GetSection("AppMessages"));
            services.AddScoped(cfg => cfg.GetService<IOptionsSnapshot<Infrastructure.Messages.ResponseMessages>>().Value);

            services.Configure<Infrastructure.Configuration.AppConfiguration>(Configuration.GetSection("AppConfiguration"));
            services.AddScoped(cfg => cfg.GetService<IOptionsSnapshot<Infrastructure.Configuration.AppConfiguration>>().Value);

            services.AddAutoMapper(typeof(Mapper.MapperProfile));

            services.AddMvc().AddNewtonsoftJson(opts =>
            {
                opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                opts.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                opts.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                opts.SerializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat;
                opts.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.fffZ";
                opts.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Piedra, papel o tijera",
                    Version = "v1",
                    Description = "Api para la prueba de EvolutionXp - Piedra, papel o tijera",
                    Contact = new OpenApiContact
                    {
                        Name = "Santiago Londo�o Le�n",
                        Email = "santiagoleon.iearm@gmail.com",
                    }
                });
            });

            services.AddCors();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PPT V1");
                c.RoutePrefix = "documentation";
            });

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
