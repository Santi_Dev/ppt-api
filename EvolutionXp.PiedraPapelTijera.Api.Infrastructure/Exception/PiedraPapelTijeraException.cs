﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Exception
{
    public class PiedraPapelTijeraException : System.Exception
    {
        public Model.Enums.MessageCode ErrorCode { get; set; }

        public PiedraPapelTijeraException(string message, Model.Enums.MessageCode code) : base(message)
        {
            ErrorCode = code;
        }
    }
}
