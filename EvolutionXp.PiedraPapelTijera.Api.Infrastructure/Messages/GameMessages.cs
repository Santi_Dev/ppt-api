﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Messages
{
    public class GameMessages
    {
        /// <summary>
        /// El juego ha finalizado
        /// </summary>
        public string Finished { get; set; }

        /// <summary>
        /// El juego aún no ha finalizado
        /// </summary>
        public string NoFinished { get; set; }

        /// <summary>
        /// El juego no tiene suficientes rounds para determinar un ganador
        /// </summary>
        public string NoEnoughRounds { get; set; }
    }
}
