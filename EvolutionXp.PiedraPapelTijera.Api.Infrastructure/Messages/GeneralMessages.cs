﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Messages
{
    public class GeneralMessages
    {
        /// <summary>
        /// El proceso se ejecuta correctamente
        /// </summary>
        public string Success { get; set; }

        /// <summary>
        /// La petición es erronea
        /// </summary>
        public string BadRequest { get; set; }

        /// <summary>
        /// El campo {0} es requerido
        /// </summary>
        public string FieldRequired { get; set; }

        /// <summary>
        /// {0} no fue encontrado
        /// </summary>
        public string NotFound { get; set; }

        /// <summary>
        /// Error del servidor, por favor comuniquese con el administrador
        /// </summary>
        public string ServerError { get; set; }
    }
}
