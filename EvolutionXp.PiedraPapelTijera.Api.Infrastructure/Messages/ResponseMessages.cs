﻿namespace EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Messages
{
    public class ResponseMessages
    {
        /// <summary>
        /// General messages
        /// </summary>
        public GeneralMessages General { get; set; }

        /// <summary>
        /// Game messages
        /// </summary>
        public GameMessages Game { get; set; }
    }
}
