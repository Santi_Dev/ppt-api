﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Extensions
{
    public static class AppExtensions
    {
        #region General extension
        public static Guid GetGuid(this string value)
        {
            if(string.IsNullOrEmpty(value))
            {
                return Guid.NewGuid();
            }

            Guid newGuid;

            Guid.TryParse(value, out newGuid);

            return newGuid;
        }
        #endregion
    }
}
