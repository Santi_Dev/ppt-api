﻿namespace EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Configuration
{
    public class AppConfiguration
    {
        public int AllowWinnerQuantity { get; set; }
    }
}
