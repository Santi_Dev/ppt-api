﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Domain.PlayerService
{
    public interface IPlayerService
    {
        /// <summary>
        /// Get player winner to the specific game
        /// </summary>
        /// <param name="gameId">Game id</param>
        /// <returns>Player winner <see cref="Model.Dtos.PlayerDto"/></returns>
        System.Threading.Tasks.Task<Model.Services.GeneralResponse<Model.Dtos.PlayerDto>> GetPlayerWinnerByGameAsync(string gameId);
    }
}
