﻿using AutoMapper;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Exception;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Extensions;
using EvolutionXp.PiedraPapelTijera.Api.Model.Dtos;
using EvolutionXp.PiedraPapelTijera.Api.Model.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionXp.PiedraPapelTijera.Api.Domain.PlayerService
{
    public class PlayerService : IPlayerService
    {
        #region Attributes

        private readonly IMapper Mapper;
        private readonly Model.Context.AppContext Context;
        private readonly Infrastructure.Messages.ResponseMessages ResponseMessages;
        private readonly Infrastructure.Configuration.AppConfiguration AppConfiguration;
        private readonly ILogger Logger;

        #endregion

        #region Constructor

        public PlayerService(Model.Context.AppContext contextInstance, 
                            Infrastructure.Messages.ResponseMessages messagesInstance, 
                            IMapper mapperInstance,
                            Infrastructure.Configuration.AppConfiguration appConfig,
                            ILogger<PlayerService> loggerInstance)
        {
            Context = contextInstance;
            ResponseMessages = messagesInstance;
            Mapper = mapperInstance;
            AppConfiguration = appConfig;
            Logger = loggerInstance;
        }

        #endregion

        #region Public Methods

        public async Task<GeneralResponse<PlayerDto>> GetPlayerWinnerByGameAsync(string gameId)
        {
            try
            {

                if(string.IsNullOrEmpty(gameId))
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "GameId"), Model.Enums.MessageCode.FieldRequired);
                }

                var gameFound = await Context.Games.Where(p => p.Id == gameId.GetGuid())
                                                   .Include(r => r.Rounds)
                                                   .ThenInclude(rr => rr.RoundResultInfo)
                                                   .ThenInclude(w => w.WinnerInfo)
                                                   .Select(g => new Model.Entities.Game()
                                                   {
                                                       Id = g.Id,
                                                       Enabled = g.Enabled,
                                                       Players = g.Players,
                                                       Rounds = g.Rounds.Where(r => r.Enabled && r.RoundResultInfo.WinnerId.HasValue).ToList()
                                                   })
                                                   .FirstOrDefaultAsync();

                if(gameFound == null)
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.NotFound, "El juego"), Model.Enums.MessageCode.NotFound);
                }

                // the game still active
                if(gameFound.Enabled)
                {
                    throw new PiedraPapelTijeraException(ResponseMessages.Game.NoFinished, Model.Enums.MessageCode.NoFinished);
                }

                // the game no has rounds
                if(gameFound.Rounds.Count < AppConfiguration.AllowWinnerQuantity)
                {
                    throw new PiedraPapelTijeraException(ResponseMessages.Game.NoEnoughRounds, Model.Enums.MessageCode.NoEnoughRounds);
                }

                // group by won rounds
                var playerWinner = gameFound.Rounds.GroupBy(r => r.RoundResultInfo.WinnerInfo.Id)
                                                   .Where(g => g.Count() == AppConfiguration.AllowWinnerQuantity)
                                                   .Select(g => g.First())
                                                   .Select(r => r.RoundResultInfo.WinnerInfo)
                                                   .FirstOrDefault();

                if(playerWinner == null)
                {
                    throw new PiedraPapelTijeraException(ResponseMessages.Game.NoEnoughRounds, Model.Enums.MessageCode.NoEnoughRounds);
                }

                return new GeneralResponse<PlayerDto>()
                {
                    Status = Model.Enums.MessageCode.Success,
                    Message = ResponseMessages.General.Success,
                    Item = Mapper.Map<PlayerDto>(playerWinner)
                };

            }
            catch (PiedraPapelTijeraException e)
            {
                return new GeneralResponse<PlayerDto>()
                {
                    Message = e.Message,
                    Status = e.ErrorCode
                };
            }
            catch (Exception e)
            {
                Logger.LogInformation(e.Message, e);

                return new GeneralResponse<PlayerDto>()
                {
                    Message = ResponseMessages.General.ServerError,
                    Status = Model.Enums.MessageCode.ServerError
                };
            }
        }

        #endregion
    }
}
