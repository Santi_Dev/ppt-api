﻿using AutoMapper;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Exception;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Extensions;
using EvolutionXp.PiedraPapelTijera.Api.Model.Dtos;
using EvolutionXp.PiedraPapelTijera.Api.Model.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionXp.PiedraPapelTijera.Api.Domain.RoundService
{
    public class RoundService : IRoundService
    {
        #region Attributes

        private readonly IMapper Mapper;
        private readonly Model.Context.AppContext Context;
        private readonly Infrastructure.Messages.ResponseMessages ResponseMessages;
        private readonly Infrastructure.Configuration.AppConfiguration AppConfiguration;
        private readonly ILogger Logger;

        #endregion

        #region Constructor

        public RoundService(Model.Context.AppContext contextInstance,
                            Infrastructure.Messages.ResponseMessages messagesInstance,
                            IMapper mapperInstance,
                            Infrastructure.Configuration.AppConfiguration configInstance,
                            ILogger<RoundService> loggerInstance)
        {
            Context = contextInstance;
            ResponseMessages = messagesInstance;
            Mapper = mapperInstance;
            AppConfiguration = configInstance;
            Logger = loggerInstance;
        }

        #endregion

        #region Public Methods

        public async Task<BaseResponse> AddRoundAsync(RoundDto roundInfo)
        {
            try
            {
                ValidateRound(roundInfo);

                var gameFound = await Context.Games.Where(g => g.Id == roundInfo.GameId.GetGuid()).Include(r => r.Rounds)
                                                                                                  .ThenInclude(rr => rr.RoundResultInfo)
                                                                                                  .FirstOrDefaultAsync();

                if (gameFound == null)
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.NotFound, "El juego"), Model.Enums.MessageCode.NotFound);
                }

                // game over
                if(!gameFound.Enabled)
                {
                    throw new PiedraPapelTijeraException(ResponseMessages.Game.Finished, Model.Enums.MessageCode.GameOver);
                }

                var newRound = Mapper.Map<Model.Entities.Round>(roundInfo);

                // get round result beetween two players
                newRound.RoundResultInfo = await GetRoundResult(roundInfo.PlayerMoves);

                // validate game winner
                if (newRound.RoundResultInfo.WinnerId.HasValue)
                {
                    // get all rounds player won
                    var playerRoundsWon = gameFound.Rounds.Where(r => r.Enabled).Count(r => r.RoundResultInfo.WinnerId.HasValue && r.RoundResultInfo.WinnerId.Value == newRound.RoundResultInfo.WinnerId);
                    // determinate if player is winner yet
                    var hasWinner = playerRoundsWon == (AppConfiguration.AllowWinnerQuantity - 1);

                    if (hasWinner)
                    {
                        // update game status
                        gameFound.Enabled = false;
                    }
                }

                Context.Rounds.Add(newRound);

                await Context.SaveChangesAsync();

                return new GeneralResponse<GameDto>()
                {
                    Message = ResponseMessages.General.Success,
                    Status = Model.Enums.MessageCode.Success
                };

            }
            catch (PiedraPapelTijeraException e)
            {
                return new GeneralResponse<GameDto>()
                {
                    Message = e.Message,
                    Status = e.ErrorCode
                };
            }
            catch (Exception e)
            {
                Logger.LogInformation(e.Message, e);

                return new GeneralResponse<GameDto>()
                {
                    Message = ResponseMessages.General.ServerError,
                    Status = Model.Enums.MessageCode.ServerError
                };
            }
        }

        #endregion

        #region Private Methods

        private void ValidateRound(RoundDto roundInfo)
        {
            if (roundInfo == null)
            {
                throw new PiedraPapelTijeraException(ResponseMessages.General.BadRequest, Model.Enums.MessageCode.BadRequest);
            }

            if (string.IsNullOrEmpty(roundInfo.GameId))
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "GameId"), Model.Enums.MessageCode.FieldRequired);
            }

            if (roundInfo.PlayerMoves == null)
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "PlayerMoves"), Model.Enums.MessageCode.FieldRequired);
            }

            if (!roundInfo.PlayerMoves.Any())
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "PlayerMoves"), Model.Enums.MessageCode.FieldRequired);
            }
        }

        private async Task<Model.Entities.RoundResult> GetRoundResult(List<PlayerMoveDto> playerMoves)
        {
            // validate winner in player moves
            for (var currentIndex = 0; currentIndex < playerMoves.Count; currentIndex++)
            {
                var currenPlayerMove = playerMoves[currentIndex];

                // validate round result
                for (var comparedIndex = 0; comparedIndex < playerMoves.Count; comparedIndex++)
                {
                    // ignore same move
                    if (currentIndex == comparedIndex)
                    {
                        continue;
                    }

                    var playerMoveToCompared = playerMoves[comparedIndex];

                    // tie
                    if (currenPlayerMove.MoveTypeId == playerMoveToCompared.MoveTypeId)
                    {
                        return new Model.Entities.RoundResult();
                    }

                    // no tie then search the winner
                    // compare moves and get winner
                    var playerWinner = await GetWinner(currenPlayerMove, playerMoveToCompared);

                    // get winner in round result
                    return new Model.Entities.RoundResult()
                    {
                        WinnerId = playerWinner.PlayerId.GetGuid()
                    };
                }
            }

            // tie
            return new Model.Entities.RoundResult();
        }

        private async Task<PlayerMoveDto> GetWinner(PlayerMoveDto currentMove, PlayerMoveDto comparedMove)
        {
            var currentMoveTypeFound = await Context.MoveTypes.Where(mt => mt.Id == currentMove.MoveTypeId.GetGuid()).Include(w => w.WeakInfo).FirstOrDefaultAsync();

            if (currentMoveTypeFound == null)
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.NotFound, "El movimiento"), Model.Enums.MessageCode.NotFound);
            }

            var comparedMoveTypeFound = await Context.MoveTypes.Where(mt => mt.Id == comparedMove.MoveTypeId.GetGuid()).Include(w => w.WeakInfo).FirstOrDefaultAsync();

            if (comparedMoveTypeFound == null)
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.NotFound, "El movimiento"), Model.Enums.MessageCode.NotFound);
            }

            // compare if current player move is weak to the compared
            if (currentMoveTypeFound.Id == comparedMoveTypeFound.WeakId)
            {
                return currentMove;
            }

            return comparedMove;
        }

        #endregion
    }
}
