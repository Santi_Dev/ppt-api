﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Domain.RoundService
{
    public interface IRoundService
    {
        /// <summary>
        /// Add new round into an existing game
        /// </summary>
        /// <param name="roundInfo">Round info <see cref="Model.Dtos.RoundDto"/></param>
        /// <returns>Base response with status <see cref="Model.Services.BaseResponse"/></returns>
        System.Threading.Tasks.Task<Model.Services.BaseResponse> AddRoundAsync(Model.Dtos.RoundDto roundInfo);
    }
}
