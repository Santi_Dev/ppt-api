﻿using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Exception;
using EvolutionXp.PiedraPapelTijera.Api.Model.Dtos;
using EvolutionXp.PiedraPapelTijera.Api.Model.Services;
using System.Threading.Tasks;
using AutoMapper;
using System.Linq;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.Logging;

namespace EvolutionXp.PiedraPapelTijera.Api.Domain.GameService
{
    public class GameService : IGameService
    {

        #region Attributes

        private readonly IMapper Mapper;
        private readonly Model.Context.AppContext Context;
        private readonly Infrastructure.Messages.ResponseMessages ResponseMessages;
        private readonly ILogger Logger;

        #endregion

        #region Constructor

        public GameService(Model.Context.AppContext contextInstance,
                           Infrastructure.Messages.ResponseMessages messagesInstance, 
                           IMapper mapperInstance,
                           ILogger<GameService> loggerInstance)
        {
            Context = contextInstance;
            ResponseMessages = messagesInstance;
            Mapper = mapperInstance;
            Logger = loggerInstance;
        }

        #endregion

        #region Public Methods

        public async Task<GeneralResponse<GameDto>> AddGameAsync(GameDto gameInfo)
        {
            try
            {
                ValidateGameInfo(gameInfo);

                var newGame = Mapper.Map<Model.Entities.Game>(gameInfo);

                Context.Games.Add(newGame);

                await Context.SaveChangesAsync();

                return new GeneralResponse<GameDto>()
                {
                    Message = ResponseMessages.General.Success,
                    Status = Model.Enums.MessageCode.Success,
                    Item = new GameDto()
                    {
                        Enabled = true,
                        Id = newGame.Id.ToString(),
                        Players = Mapper.Map<List<PlayerDto>>(newGame.Players)
                    }
                };

            }
            catch (PiedraPapelTijeraException e)
            {
                return new GeneralResponse<GameDto>()
                {
                    Message = e.Message,
                    Status = e.ErrorCode
                };
            }
            catch (Exception e)
            {
                Logger.LogInformation(e.Message, e);

                return new GeneralResponse<GameDto>()
                {
                    Message = ResponseMessages.General.ServerError,
                    Status = Model.Enums.MessageCode.ServerError
                };
            }
        }

        public async Task<GeneralResponse<GameDto>> GetGameByIdAsync(string gameId)
        {
            try
            {
                if(string.IsNullOrEmpty(gameId))
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "GameId"), Model.Enums.MessageCode.FieldRequired);
                }

                var gameFound = await Context.Games.Where(g => g.Id == gameId.GetGuid()).Include(p => p.Players)
                                                                                        .Include(r => r.Rounds)
                                                                                        .ThenInclude(pm => pm.RoundResultInfo)
                                                                                        .ThenInclude(w => w.WinnerInfo)
                                                                                        .Select(g => new Model.Entities.Game()
                                                                                        {
                                                                                            Players = g.Players,
                                                                                            Id = g.Id,
                                                                                            Enabled = g.Enabled,
                                                                                            Rounds = g.Rounds.Where(r => r.Enabled).ToList()
                                                                                        })
                                                                                        .FirstOrDefaultAsync();
                if(gameFound == null)
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.NotFound, "El juego"), Model.Enums.MessageCode.NotFound);
                }

                return new GeneralResponse<GameDto>()
                {
                    Message = ResponseMessages.General.Success,
                    Status = Model.Enums.MessageCode.Success,
                    Item = Mapper.Map<GameDto>(gameFound)
                };
            }
            catch (PiedraPapelTijeraException e)
            {
                return new GeneralResponse<GameDto>()
                {
                    Message = e.Message,
                    Status = e.ErrorCode
                };
            }
            catch (Exception e)
            {
                Logger.LogInformation(e.Message, e);

                return new GeneralResponse<GameDto>()
                {
                    Message = ResponseMessages.General.ServerError,
                    Status = Model.Enums.MessageCode.ServerError
                };
            }
        }

        public async Task<BaseResponse> ResetGameAsync(string gameId)
        {
            try
            {
                if (string.IsNullOrEmpty(gameId))
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "GameId"), Model.Enums.MessageCode.FieldRequired);
                }

                var gameFound = await Context.Games.Where(g => g.Id == gameId.GetGuid()).Include(r => r.Rounds)
                                                                                        .FirstOrDefaultAsync();
                if (gameFound == null)
                {
                    throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.NotFound, "El juego"), Model.Enums.MessageCode.NotFound);
                }

                // the game still active
                if (gameFound.Enabled)
                {
                    throw new PiedraPapelTijeraException(ResponseMessages.Game.NoFinished, Model.Enums.MessageCode.NoFinished);
                }

                // enabled game
                gameFound.Enabled = true;

                // disabled rounds
                gameFound.Rounds = gameFound.Rounds.Select(r => {
                    r.Enabled = false;
                    return r;
                }).ToList();

                await Context.SaveChangesAsync();

                return new BaseResponse()
                {
                    Message = ResponseMessages.General.Success,
                    Status = Model.Enums.MessageCode.Success,
                };
            }
            catch (PiedraPapelTijeraException e)
            {
                return new BaseResponse()
                {
                    Message = e.Message,
                    Status = e.ErrorCode
                };
            }
            catch (Exception e)
            {
                Logger.LogInformation(e.Message, e);

                return new BaseResponse()
                {
                    Message = ResponseMessages.General.ServerError,
                    Status = Model.Enums.MessageCode.ServerError
                };
            }
        }

        #endregion

        #region Private Methods
        private void ValidateGameInfo(GameDto gameInfo)
        {
            if(gameInfo == null)
            {
                throw new PiedraPapelTijeraException(ResponseMessages.General.BadRequest, Model.Enums.MessageCode.BadRequest);
            }

            if (gameInfo.Players == null)
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "Players"), Model.Enums.MessageCode.FieldRequired);
            }

            if (!gameInfo.Players.Any())
            {
                throw new PiedraPapelTijeraException(string.Format(ResponseMessages.General.FieldRequired, "Players"), Model.Enums.MessageCode.FieldRequired);
            }
        }
        #endregion
    }
}
