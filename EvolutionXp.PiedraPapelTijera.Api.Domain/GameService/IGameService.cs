﻿namespace EvolutionXp.PiedraPapelTijera.Api.Domain.GameService
{
    public interface IGameService
    {
        /// <summary>
        /// Create new game
        /// </summary>
        /// <param name="gameInfo">Game info</param>
        /// <returns>Base response<see cref="Model.Services.GeneralResponse{T}"/></returns>
        System.Threading.Tasks.Task<Model.Services.GeneralResponse<Model.Dtos.GameDto>> AddGameAsync(Model.Dtos.GameDto gameInfo);

        /// <summary>
        /// Get game by id
        /// </summary>
        /// <param name="gameId">Game id</param>
        /// <returns>General response with game <see cref="Model.Dtos.GameDto"/></returns>
        System.Threading.Tasks.Task<Model.Services.GeneralResponse<Model.Dtos.GameDto>> GetGameByIdAsync(string gameId);

        /// <summary>
        /// Reset game
        /// </summary>
        /// <param name="gameId">Game id</param>
        /// <returns>Base response <see cref="Model.Services.BaseResponse"/></returns>
        System.Threading.Tasks.Task<Model.Services.BaseResponse> ResetGameAsync(string gameId);
    }
}
