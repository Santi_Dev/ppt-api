﻿namespace EvolutionXp.PiedraPapelTijera.Api.Domain.MoveTypeService
{
    public interface IMoveTypeService
    {
        /// <summary>
        /// Get move types
        /// </summary>
        /// <returns>Base response <see cref="Model.Services.GeneralResponse{T}"/></returns>
        System.Threading.Tasks.Task<Model.Services.GeneralResponse<Model.Dtos.MoveTypeDto>> GetMoveTypesAsync();
    }
}
