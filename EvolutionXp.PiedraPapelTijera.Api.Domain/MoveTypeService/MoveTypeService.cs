﻿using AutoMapper;
using EvolutionXp.PiedraPapelTijera.Api.Infrastructure.Exception;
using EvolutionXp.PiedraPapelTijera.Api.Model.Dtos;
using EvolutionXp.PiedraPapelTijera.Api.Model.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace EvolutionXp.PiedraPapelTijera.Api.Domain.MoveTypeService
{
    public class MoveTypeService : IMoveTypeService
    {
        #region Attributes

        private readonly IMapper Mapper;
        private readonly Model.Context.AppContext Context;
        private readonly Infrastructure.Messages.ResponseMessages ResponseMessages;
        private readonly ILogger Logger;

        #endregion

        #region Constructor

        public MoveTypeService(Model.Context.AppContext contextInstance,
                               Infrastructure.Messages.ResponseMessages messagesInstance,
                               IMapper mapperInstance,
                               ILogger<MoveTypeService> loggerInstance)
        {
            Context = contextInstance;
            ResponseMessages = messagesInstance;
            Mapper = mapperInstance;
            Logger = loggerInstance;
        }

        #endregion

        #region Public Methods

        public async Task<GeneralResponse<MoveTypeDto>> GetMoveTypesAsync()
        {
            try
            {
                var movesTypesFound = await Context.MoveTypes.OrderBy(m => m.Order).ToListAsync();

                return new GeneralResponse<MoveTypeDto>()
                {
                    Message = ResponseMessages.General.Success,
                    Status = Model.Enums.MessageCode.Success,
                    List = Mapper.Map<List<MoveTypeDto>>(movesTypesFound)
                };
            }
            catch (PiedraPapelTijeraException e)
            {
                return new GeneralResponse<MoveTypeDto>()
                {
                    Message = e.Message,
                    Status = e.ErrorCode
                };
            }
            catch (Exception e)
            {
                Logger.LogInformation(e.Message, e);

                return new GeneralResponse<MoveTypeDto>()
                {
                    Message = ResponseMessages.General.ServerError,
                    Status = Model.Enums.MessageCode.ServerError
                };
            }
        }

        #endregion
    }
}
