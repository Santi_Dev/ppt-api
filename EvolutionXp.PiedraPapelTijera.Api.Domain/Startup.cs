﻿using Microsoft.Extensions.DependencyInjection;

namespace EvolutionXp.PiedraPapelTijera.Api.Domain
{
    public static class Startup
    {
        /// <summary>
        /// Configuration services dependency injection
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            // add services here
            services.AddTransient<GameService.IGameService, GameService.GameService>();
            services.AddTransient<RoundService.IRoundService, RoundService.RoundService>();
            services.AddTransient<PlayerService.IPlayerService, PlayerService.PlayerService>();
            services.AddTransient<MoveTypeService.IMoveTypeService, MoveTypeService.MoveTypeService>();

            return services;
        }
    }
}
