﻿CREATE TABLE [dbo].[RoundResult]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [RoundId] UNIQUEIDENTIFIER NOT NULL, 
    [WinnerId] UNIQUEIDENTIFIER NULL,  
    [CreatedDate] DATETIME NOT NULL DEFAULT getutcdate(),  
    CONSTRAINT [FK_PlayerMove_Round] FOREIGN KEY ([RoundId]) REFERENCES [Round]([Id]), 
    CONSTRAINT [FK_PlayerMove_Player] FOREIGN KEY ([WinnerId]) REFERENCES [Player]([Id])
)
