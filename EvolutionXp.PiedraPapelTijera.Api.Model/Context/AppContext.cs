﻿using EvolutionXp.PiedraPapelTijera.Api.Model.Entities;
using Microsoft.EntityFrameworkCore;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Context
{
    public class AppContext : DbContext
    {
        #region Tables

        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<MoveType> MoveTypes { get; set; }
        public DbSet<Round> Rounds { get; set; }
        public DbSet<RoundResult> RoundResults { get; set; }

        #endregion

        #region Configurations

        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region foreign keys configuration

            modelBuilder.Entity<Game>()
                        .HasMany(g => g.Players)
                        .WithOne(p => p.GameInfo)
                        .HasForeignKey(p => p.GameId);

            modelBuilder.Entity<Game>()
                        .HasMany(g => g.Rounds)
                        .WithOne(r => r.GameInfo)
                        .HasForeignKey(r => r.GameId);

            modelBuilder.Entity<MoveType>()
                        .HasOne(g => g.WeakInfo)
                        .WithOne()
                        .HasForeignKey<MoveType>(g => g.WeakId);

            modelBuilder.Entity<Round>()
                        .HasOne(r => r.RoundResultInfo)
                        .WithOne()
                        .HasForeignKey<RoundResult>(rs => rs.RoundId);

            modelBuilder.Entity<Player>()
                        .HasMany(r => r.RoundResults)
                        .WithOne(p => p.WinnerInfo)
                        .HasForeignKey(p => p.WinnerId);

            #endregion

            #region default properties

            modelBuilder.Entity<Game>().Property(g => g.CreatedDate).HasDefaultValueSql("getutcgate()");
            modelBuilder.Entity<RoundResult>().Property(g => g.CreatedDate).HasDefaultValueSql("getutcgate()");
            modelBuilder.Entity<Round>().Property(g => g.Enabled).HasDefaultValue(1);
            modelBuilder.Entity<Game>().Property(g => g.Enabled).HasDefaultValue(1);

            #endregion

        }

        #endregion
    }
}
