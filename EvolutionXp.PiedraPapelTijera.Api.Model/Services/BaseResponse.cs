﻿using EvolutionXp.PiedraPapelTijera.Api.Model.Enums;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Services
{
    public class BaseResponse
    {
        public BaseResponse()
        {
            Status = MessageCode.Success;
        }

        public MessageCode Status { get; set; }
        public string Message { get; set; }
    }
}
