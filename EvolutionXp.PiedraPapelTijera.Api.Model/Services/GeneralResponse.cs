﻿using System.Collections.Generic;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Services
{
    public class GeneralResponse<T> : BaseResponse
    {
        public T Item { get; set; }
        public List<T> List { get; set; }
    }
}
