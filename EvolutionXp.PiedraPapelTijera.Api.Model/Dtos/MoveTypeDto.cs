﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Model.Dtos
{
    public class MoveTypeDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
