﻿using System.Collections.Generic;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Dtos
{
    public class RoundDto
    {
        public string Id { get; set; }
        public string GameId { get; set; }
        public RoundResultDto RoundResultInfo { get; set; }
        public List<PlayerMoveDto> PlayerMoves { get; set; }
    }
}
