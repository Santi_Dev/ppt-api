﻿using System;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Dtos
{
    public class PlayerMoveDto
    {
        public string Id { get; set; }
        public string PlayerId { get; set; }
        public string MoveTypeId { get; set; }
    }
}
