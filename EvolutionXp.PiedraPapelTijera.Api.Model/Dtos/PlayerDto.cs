﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Model.Dtos
{
    public class PlayerDto
    {
        public string Id { get; set; }
        public string GameId { get; set; }
        public string Name { get; set; }
    }
}
