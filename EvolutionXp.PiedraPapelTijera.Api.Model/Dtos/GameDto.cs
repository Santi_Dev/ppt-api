﻿using System;
using System.Collections.Generic;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Dtos
{
    public class GameDto
    {
        public string Id { get; set; }
        public bool Enabled { get; set; }
        public List<PlayerDto> Players { get; set; }
        public List<RoundDto> Rounds { get; set; }
    }
}
