﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Model.Dtos
{
    public class RoundResultDto
    {
        public string Id { get; set; }
        public PlayerDto WinnerInfo { get; set; }
    }
}
