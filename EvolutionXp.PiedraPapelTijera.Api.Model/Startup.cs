﻿using EvolutionXp.PiedraPapelTijera.Api.Model.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EvolutionXp.PiedraPapelTijera.Api.Model
{
    public static class Startup
    {
        public static IServiceCollection AddModels(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<AppContext>(options =>
                options.UseSqlServer(connectionString));

            return services;
        }
    }
}
