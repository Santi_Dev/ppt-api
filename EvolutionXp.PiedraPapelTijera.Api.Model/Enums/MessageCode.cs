﻿
namespace EvolutionXp.PiedraPapelTijera.Api.Model.Enums
{
    public enum MessageCode
    {
        Success = 200,
        BadRequest = 400,
        ServerError = 500,
        FieldRequired = 3000,
        NotFound = 3001,

        // Game - 4000 - 4100
        GameOver = 4000,
        NoFinished = 4001,
        NoEnoughRounds = 4002
    }
}
