﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Entities
{
    [Table("Game")]
    public class Game
    {
        [Key]
        public Guid Id { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedDate { get; set; }

        // Navigation properties
        public List<Player> Players { get; set; }
        public List<Round> Rounds { get; set; }
    }
}
