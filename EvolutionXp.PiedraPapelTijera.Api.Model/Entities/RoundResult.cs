﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Entities
{
    [Table("RoundResult")]
    public class RoundResult
    {
        [Key]
        public Guid Id { get; set; }
        public Guid RoundId { get; set; }
        public Guid? WinnerId { get; set; }
        public DateTime CreatedDate { get; set; }

        // Navigation properties
        public Player WinnerInfo { get; set; }
    }
}
