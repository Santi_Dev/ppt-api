﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Entities
{
    [Table("Player")]
    public class Player
    {
        [Key]
        public Guid Id { get; set; }
        public Guid GameId { get; set; }
        public string Name { get; set; }

        // Navigation properties
        public Game GameInfo { get; set; }
        public List<RoundResult> RoundResults { get; set; }
    }
}
