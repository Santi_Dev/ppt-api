﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Entities
{
    [Table("MoveType")]
    public class MoveType
    {
        [Key]
        public Guid Id { get; set; }
        public Guid WeakId { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        // Navigation properties
        public MoveType WeakInfo { get; set; }
    }
}
