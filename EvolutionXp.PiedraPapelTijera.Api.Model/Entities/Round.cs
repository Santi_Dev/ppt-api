﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EvolutionXp.PiedraPapelTijera.Api.Model.Entities
{
    [Table("Round")]
    public class Round
    {
        [Key]
        public Guid Id { get; set; }
        public Guid GameId { get; set; }
        public bool Enabled { get; set; }

        // Navigation properties
        public Game GameInfo { get; set; }
        public RoundResult RoundResultInfo { get; set; }
    }
}
